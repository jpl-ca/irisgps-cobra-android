package jmt.irisgps.tracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import jmt.irisgps.cobratrack.dialog.InfoPersonalDialog;
import jmt.irisgps.tracker.dialog.RegisterIncidentDialog;
import jmt.irisgps.tracker.interfaz.CallbackDialog;
import jmt.irisgps.tracker.model.DA.TasksDA;
import jmt.irisgps.tracker.model.DA.UserAccountDA;
import jmt.irisgps.tracker.model.entity.CustomerE;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.TasksE;
import jmt.irisgps.tracker.model.entity.UserAccountE;
import jmt.irisgps.tracker.service.LocationFusedService;

import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class TrackMapActivity extends ActionBarActivity implements OnMapReadyCallback , GoogleMap.OnMarkerClickListener{
    private GoogleMap map;
    Toolbar toolbar;
    Marker markerME;
    LatLng ME;
    GpsReceiver myReceiver;
    Intent intentGPS;
    List<TasksE> taskList;
    PolylineOptions optionsLine;
    Polyline lineMe;
    TasksDA tasksDA;
    UserAccountE usuario;
    Marker marker[];
    HashMap<String,Integer> HashCli;
    int REQUEST_CODE;
    int total_positions = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_map);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.ic_toolbar);
        optionsLine = new PolylineOptions().width(4).color(Color.parseColor("#e74c3c")).geodesic(true);
        tasksDA = new TasksDA();
        usuario = new UserAccountDA().select();
        if(usuario!=null)
             getSupportActionBar().setTitle("  "+usuario.getFirst_name()+" "+usuario.getLast_name());
        else System.out.println(" No hay Usuario - No hay Usuario No hay Usuario - No hay Usuario");
    }
    @Override
    protected void onStart() {
        super.onStart();
        intentGPS = new Intent(this, LocationFusedService.class);
        startService(intentGPS);

        myReceiver = new GpsReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LocationFusedService.NEW_GPS);
        registerReceiver(myReceiver, intentFilter);

        turnGPSOn();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        ME = new LatLng(-12.089555406684041,-77.01358316161633);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 12));
        map.animateCamera(CameraUpdateFactory.zoomTo(12), 500, null);
//        MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place)).position(ME);
//        markerME = map.addMarker(mo);
        loadInfoRoutes();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadInfoRoutes();
    }

    private class GpsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            System.out.println("Upd in map!");
            Bundle extra = arg1.getExtras();
            if(extra!=null){
                Double Lat = extra.getDouble("latitude");
                Double Lng = extra.getDouble("longitude");
//                float accuracy = extra.getFloat("accuracy");
//                String provider = extra.getString("provider");
//                String tit = Lat+","+Lng;
//                ((TextView)findViewById(R.id.txtLL)).setText(tit);
//                ((TextView)findViewById(R.id.txtLL3)).setText(accuracy+"  -  "+provider);
                ME =  new LatLng(Lat,Lng);
                if(markerME!=null) markerME.remove();
                MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_man_place)).position(ME);
                markerME = map.addMarker(mo);
//                if(lineMe!=null) lineMe.remove();
//                optionsLine.add(ME);
//                lineMe = map.addPolyline(optionsLine);
//                map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 14));
//                map.animateCamera(CameraUpdateFactory.zoomTo(14), 500, null);
//                updateWithFix(Lat,Lng);
            }
        }
    }

    private void loadInfoRoutes() {
        final LatLngBounds.Builder builderLL = new LatLngBounds.Builder();
        HashCli = new HashMap<>();
        taskList = tasksDA.select();
        if(marker!=null)for (int i=0;i<marker.length;i++)marker[i].remove();
        marker = new Marker[taskList.size()];
        boolean IS_FINISH=true;
        total_positions = 0;
        for (int i=0;i<taskList.size();i++){
            long task_st_id = taskList.get(i).getTask_state_id();
            if(task_st_id==S.TASK_STATE.PROGRAMADA||task_st_id==S.TASK_STATE.POSPUESTA)
                IS_FINISH = false;
            CustomerE cli = taskList.get(i).getCustomer();
            int ic_loc = 0;
            int task_state = (int)taskList.get(i).getTask_state_id();
            if(task_state==S.TASK_STATE.PROGRAMADA)
                ic_loc = R.drawable.ic_place_purple;
            else if(task_state==S.TASK_STATE.REALIZADA)
                ic_loc = R.drawable.ic_place_green;
            else if(task_state==S.TASK_STATE.POSPUESTA)
                ic_loc = R.drawable.ic_place_orange;
            else if(task_state==S.TASK_STATE.CANCELADA)
                ic_loc = R.drawable.ic_place_gray;
            else
                ic_loc = R.drawable.ic_place_pistachio;
            LatLng ll = new LatLng(cli.getLat(),cli.getLng());
            MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory
                    .fromResource(ic_loc)).position(ll).title(cli.getName());
            marker[i] = map.addMarker(mo);
            HashCli.put(marker[i].getId(),i);
            total_positions++;
            builderLL.include(ll);
        }
        if(taskList.size()>0)
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition arg0) {
                    builderLL.include(ME);
                    System.out.println(total_positions+"::::::::::::");
                    System.out.println(total_positions+"::::::::::::");
//                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builderLL.build(), 50));
//                    map.setOnCameraChangeListener(null);
                }
            });
        usuario.setRoutes_completed(IS_FINISH);
        usuario.save();
        map.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(HashCli.containsKey(marker.getId())){
            Intent it= new Intent(this,TaskCustomerActivity.class);
            TasksE task = taskList.get(HashCli.get(marker.getId()));
            it.putExtra(S.TASK.id,task.getId());
            it.putExtra(S.TASK.route_task_id,task.getTask_id());
            it.putExtra(S.POSITION.latitude,ME.latitude);
            it.putExtra(S.POSITION.longitude,ME.longitude);
            startActivity(it);
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_track_map, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_salir) {
            new Util(this).finalizarSesion(true);
//            stopService(intentGPS);
            return true;
        }
        if (id == R.id.action_incident) {
            registroIncidente();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void registroIncidente() {
        new RegisterIncidentDialog(this).show(this,ME,new CallbackDialog() {
            @Override
            public void finish(String... params) {
                System.out.println("This a greeting!!");
            }
        });
    }
    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
    }
    private void turnGPSOn(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")){
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, REQUEST_CODE);
            Toast.makeText(this,getString(R.string.s_activar_gps),Toast.LENGTH_LONG).show();
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
            }else{
                Toast.makeText(this,getString(R.string.s_activar_gps),Toast.LENGTH_LONG).show();
            }
        }
    }



    Polyline lineaG;
    ArrayList<LocationHistoriesE> locTMP;
    private void updateWithFix(Double lat, Double lng) {
        if(locTMP==null)locTMP = new ArrayList<>();
        locTMP.add(new LocationHistoriesE(lat,lng));
        ArrayList<LocationHistoriesE> locationsA = Util.posCorrectionC(locTMP, 30);
        if(lineaG!=null) lineaG.remove();
        PolylineOptions optionsLineGreen = new PolylineOptions().width(4).color(Color.parseColor("#FF10488D")).geodesic(true);
        for (LocationHistoriesE lh : locationsA){
            LatLng Mll = new LatLng(lh.getLatitud(),lh.getLongitud());
            optionsLineGreen.add(Mll);
        }
        lineaG = map.addPolyline(optionsLineGreen);
    }
}