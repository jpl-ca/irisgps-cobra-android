package jmt.irisgps.tracker.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;

import jmt.irisgps.tracker.R;
import jmt.irisgps.tracker.http_service.task.SendBatchLocationTask;
import jmt.irisgps.tracker.interfaz.CallbackDialog;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.DA.LocationHistoryDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.ResponseE;
//import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by JMTech-Android on 22/05/2015.
 */
public class RegisterIncidentDialog extends MaterialDialog implements View.OnClickListener{
    Context ctx;
    EditText txt_incidencia;
    int IncidentTypeSelected;
    InputMethodManager imm;
    Spinner spinner;
    public RegisterIncidentDialog(Context context) {
        super(context);
        LayoutInflater factory = LayoutInflater.from(context);
        View myView = factory.inflate(R.layout.dialog_register_incident, null);
        setContentView(myView);
        txt_incidencia = (EditText)myView.findViewById(R.id.txt_incidencia);
        spinner = (Spinner) myView.findViewById(R.id.sp_state);
        setTitle(context.getString(R.string.s_registrar_incidente));
        ctx = context;
        final String[] items = ctx.getResources().getStringArray(R.array.tipo_incidente);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        imm = (InputMethodManager)ctx.getSystemService(
                Context.INPUT_METHOD_SERVICE);
    }
    public void show(final Activity activity,final LatLng position, final CallbackDialog _callback){
        final LocationHistoryDA route_pos = new LocationHistoryDA();
        setPositiveButton(ctx.getString(R.string.enviar).toUpperCase(),new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(txt_incidencia.getWindowToken(), 0);
                String incidencia = txt_incidencia.getText().toString();
                IncidentTypeSelected = spinner.getSelectedItemPosition();
                if(IncidentTypeSelected==0){
                    Toast.makeText(ctx, ctx.getString(R.string.s_seleccione_un_tipo_de_incidente), Toast.LENGTH_LONG).show();
                }else if(incidencia.length()<=3){
                    Toast.makeText(ctx,ctx.getString(R.string.s_descripcion_muy_corta),Toast.LENGTH_LONG).show();
                }else{
                    txt_incidencia.setEnabled(false);
                    _callback.finish();
                    route_pos.insert(new LocationHistoriesE(position.latitude, position.longitude, String.valueOf(IncidentTypeSelected), incidencia));
                    SendBatchLocationTask batchTask = new SendBatchLocationTask(activity,new CallbackRequest() {
                        @Override
                        public void processFinish() {
                            dismiss();
                        }
                        @Override
                        public void processFinish(ResponseE response) {
                            txt_incidencia.setEnabled(true);
                            dismiss();
                        }
                    });
                    batchTask.execute();
                }
            }
        });
        setNegativeButton(ctx.getString(R.string.cancelar).toUpperCase(),new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(txt_incidencia.getWindowToken(), 0);
                dismiss();
            }
        });
        show();
    }
    @Override
    public void onClick(View v) {
//        final CharSequence[] items = ctx.getResources().getStringArray(R.array.tipo_incidente);
//        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
//        builder.setTitle(ctx.getString(R.string.seleccione_tipo_incidente));
//        builder.setSingleChoiceItems(items,0,
//                new DialogInterface.OnClickListener(){
//                    public void onClick(DialogInterface dialog, int item) {
//                        IncidentTypeSelected = item+1;
//                        tv_tipo_incidente.setText(items[item]);
//                        tv_tipo_incidente.setTypeface(null, Typeface.BOLD);
//                        levelDialog.dismiss();
//                    }
//                });
//        levelDialog = builder.create();
//        levelDialog.show();
    }
}