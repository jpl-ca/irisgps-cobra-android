package jmt.irisgps.tracker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import jmt.irisgps.tracker.service.LocationFusedService;

/**
 * Created by JMTech-Android on 29/05/2015.
 */
public class StartGpsBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, LocationFusedService.class);
            context.startService(serviceIntent);
        }
    }
}