package jmt.irisgps.tracker.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import java.util.Arrays;
import java.util.Date;

import jmt.irisgps.tracker.http_service.task.SendBatchLocationTask;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.DA.LocationHistoryDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
//    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;
    private long LAST_TIME_UPDATED = 0;

    LocationHistoryDA positionDA;
    int totalLocations;
    public final static int TOTAL_PRE_LOC = 3;
    LatLng preLocation[];

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("101*********************************************************CREATE*********************************************************");
        mRequestingLocationUpdates = false;
        intent = new Intent();
        positionDA = new LocationHistoryDA();
        totalLocations = 0;
        preLocation = new LatLng[TOTAL_PRE_LOC];
        Arrays.fill(preLocation, new LatLng(0, 0));
        // Kick off the process of building a GoogleApiClient and requesting the LocationServices
        // API.
        buildGoogleApiClient();
        mGoogleApiClient.connect();

        turnGPSOn();
    }
    private void turnGPSOn(){
//        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.putExtra("enabled", true);
        sendBroadcast(intent);
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("101.0**********************************************************COMANDDDDDDDDDDDDDD*********************************************************");
        if (mGoogleApiClient.isConnected()) {
            System.out.println("101.1**********************************************************COMANDDDDDDDDDDDDDD*********************************************************");
            startLocationUpdates();
            mRequestingLocationUpdates = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
//        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.putExtra("enabled", false);
        sendBroadcast(intent);
    }

    protected void startLocationUpdates() {
        System.out.println("Starting Positn GPS!!!");
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        System.out.println("Starting Position GPS!!!");
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            updateUI();
        }
        startLocationUpdates();
    }
    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("Connection suspended");
        mGoogleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        System.out.println("ACCURACY->"+mCurrentLocation.getAccuracy()+" : "+location.getLatitude()+","+location.getLongitude());
        updateUI();
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }
    private void updateUI() {
        if (mCurrentLocation != null) {
            /**
             * ENVIAR A LA ACTIVIDAD PRINCIPAL PARA MOSTRAR AL USUARIO
             */
            intent.putExtra(S.POSITION.latitude,mCurrentLocation.getLatitude());
            intent.putExtra(S.POSITION.longitude,mCurrentLocation.getLongitude());
            intent.putExtra("accuracy",mCurrentLocation.getAccuracy());
            intent.putExtra("provider", mCurrentLocation.getProvider());
            intent.putExtra("totalLocations", totalLocations);
            intent.setAction(NEW_GPS);
            sendBroadcast(intent);
            System.out.println("Sent!!!!");
//            if(true)return;
            /**
             * EVALUAR SI LA LOCALIZACION SE HA REPETIDO
             */
            for (int i=0;i<TOTAL_PRE_LOC;i++)
                if(preLocation[i].latitude==mCurrentLocation.getLatitude()&&preLocation[i].longitude==mCurrentLocation.getLongitude())return;

            /**
             * EVALUAR SI LA DISTANCIA ES VALIDAD PARA REGISTRAR O ESTA DEMASIADO CERCA PARA EVITAR REDUNDANCIAS
             */
            boolean b = isDistanceValid(preLocation[0].latitude,preLocation[0].longitude,mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
            System.out.println("Is distance valid?"+b);
            if(!b)return;
            /**
             * REGISTRAR EN LA BD LOCAL
             */
            totalLocations = positionDA.insert(new LocationHistoriesE(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
            /**
             * ENVIAR AUTOMATICAMENTE,SI COMO MINIMO HAY 7 DATOS REGISTRADOS EN LA BD LOCAL
             */
            System.out.println("TOTAL REGISTRADOS:"+totalLocations);
            long CURRENT_TIME = new Date().getTime();
            System.out.println((CURRENT_TIME-LAST_TIME_UPDATED)+" Ago");
            if(totalLocations>7||CURRENT_TIME-LAST_TIME_UPDATED>5*60*1000){
                LAST_TIME_UPDATED = CURRENT_TIME;
                System.out.println("LAST_TIME_UPDATED----"+LAST_TIME_UPDATED);
                new SendBatchLocationTask(this,new CallbackRequest() {
                    @Override
                    public void processFinish() {}
                    @Override
                    public void processFinish(ResponseE response) {
                        System.out.println("Enviado.....!!!!");
                    }
                }).execute();
            }
            /**
             * ACTUALIZAR LAS ULTIMAS POSICIONES
             */
            for (int i=TOTAL_PRE_LOC-1;i>0;i--)
                preLocation[i] = preLocation[i-1];
            preLocation[0] = new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
            System.out.println(Arrays.deepToString(preLocation));
        }
    }
    public boolean isDistanceValid(double last_lat, double last_lng, double current_lat, double current_lng){
        double minDistance = 5;
        double distance = 6371 * (2 * Math.atan2(Math.sqrt((Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) * Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) + Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.cos((last_lat * Math.PI / 180)) * Math.cos((current_lat * Math.PI / 180)))), Math.sqrt(1-(Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) * Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) + Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.cos((last_lat * Math.PI / 180)) * Math.cos((current_lat * Math.PI / 180))))))*1000;
//        System.out.println("Evaluando si: "+distance+", es mayor igual que: "+minDistance);
        if(distance >= minDistance)
            return true;
        return false;
    }
}