package jmt.irisgps.tracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import jmt.irisgps.tracker.http_service.task.LoadCheckInfoTask;
import jmt.irisgps.tracker.http_service.task.LoadRouteInfoTask;
import jmt.irisgps.tracker.http_service.task.LoginTask;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.DA.UserAccountDA;
import jmt.irisgps.tracker.model.entity.ResponseE;

public class SplashActivity extends ActionBarActivity {
    ImageView iv_logo;
    LinearLayout ll_login,ll_bienvenido;
    Button btn_accept;
    EditText txt_user,txt_pass;
    Animation fadeIn;
    Animation fadeOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setInitCompoents();
    }

    private void setInitCompoents() {
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        iv_logo = (ImageView)findViewById(R.id.iv_logo);
        ll_login = (LinearLayout)findViewById(R.id.ll_login);
        ll_login.setVisibility(View.INVISIBLE);
        ll_bienvenido = (LinearLayout)findViewById(R.id.ll_bienvenido);
        btn_accept = (Button)findViewById(R.id.btn_accept);
        txt_user = (EditText)findViewById(R.id.txt_user);
        txt_pass = (EditText)findViewById(R.id.txt_pass);
        txt_user.setText("");
        txt_pass.setText("");
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadCheckInfoTask();
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txt_user.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(txt_pass.getWindowToken(), 0);
                btn_accept.setEnabled(false);
                ll_login.setAnimation(fadeOut);
                ll_login.setVisibility(View.GONE);
                ll_bienvenido.setVisibility(View.VISIBLE);
                ll_bienvenido.setAnimation(fadeIn);
                new LoginTask(SplashActivity.this,new CallbackRequest() {
                    @Override
                    public void processFinish() {
                        btn_accept.setEnabled(true);
                        txt_pass.setText("");
                        ll_login.setAnimation(fadeIn);
                        ll_login.setVisibility(View.VISIBLE);
                        ll_bienvenido.setVisibility(View.GONE);
                        ll_bienvenido.setAnimation(fadeOut);
                    }
                    @Override
                    public void processFinish(ResponseE response) {
                        new UserAccountDA().insert(response.getData());
                        loadRouteInfoTask();
                    }
                }).execute(txt_user.getText().toString(),txt_pass.getText().toString());
            }
        });
    }

    public void loadCheckInfoTask(){
        new LoadCheckInfoTask(this, new CallbackRequest() {
            @Override
            public void processFinish() {
                showLogin();
            }
            @Override
            public void processFinish(ResponseE response) {
                ll_login.setVisibility(View.GONE);
                ll_bienvenido.setAnimation(fadeIn);
                ll_bienvenido.setVisibility(View.VISIBLE);
                loadRouteInfoTask();
            }
        }).execute();
    }
    public void loadRouteInfoTask(){
        new LoadRouteInfoTask(this, new CallbackRequest() {
            @Override
            public void processFinish() {
                showLogin();
            }
            @Override
            public void processFinish(ResponseE response) {
                ll_bienvenido.setAnimation(fadeIn);
                ll_bienvenido.setVisibility(View.VISIBLE);
                showWelcome();
            }
        }).execute();
    }

    public void showWelcome(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this,TrackMapActivity.class));
                finish();
            }
        },1000);
    }

    public void showLogin(){
        ll_login.setAnimation(fadeIn);
        ll_login.setVisibility(View.VISIBLE);
        ll_bienvenido.setVisibility(View.GONE);
        ll_bienvenido.setAnimation(fadeOut);
    }
}