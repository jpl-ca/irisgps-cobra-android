package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import java.io.IOException;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.Jmstore;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class LoadCheckInfoTask extends AsyncTask<String, String, ResponseE>{
    Activity ctx;
    CallbackRequest delegate;
    GoogleCloudMessaging gcm;
    String regId;
    Jmstore jmstore;
    public LoadCheckInfoTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        regId="";
        gcm = GoogleCloudMessaging.getInstance(this.ctx);
        jmstore = new Jmstore(ctx);
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        TelephonyManager tMgr = (TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        String IMEI = tMgr.getDeviceId();
        if (IMEI==null) IMEI = "101010101010101010101";
        if(mPhoneNumber!=null&&mPhoneNumber.trim().length()>0)jmstore.push(S.mobile_number, mPhoneNumber);
        if(IMEI.trim().length()>0)jmstore.push(S.IMEI, IMEI);
        regId = jmstore.get(S.GCMID);
//        if(new UserAccountDA().Count()==0){
//            Z.setSuccess(false);
//            return Z;
//        }
        try {
            if (gcm == null)gcm = GoogleCloudMessaging.getInstance(ctx);
            regId = gcm.register(S.GOOGLE.PROJECT_ID);
        }catch (IOException ex){
        }
        try {
            Z = new OkHttp(ctx).makeGetRequest("/ws/auth/auth-check");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        if(response.isSuccess()){
            delegate.processFinish(response);
        }else{
            if(response.getCode()==S.RESPONSE.irs_without_connection)
                Toast.makeText(ctx,response.getMassege(),Toast.LENGTH_LONG).show();
            else jmstore.push(S.CookieIris,"");
            delegate.processFinish();
        }
    }
}