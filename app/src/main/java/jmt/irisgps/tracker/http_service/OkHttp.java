package jmt.irisgps.tracker.http_service;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;

import jmt.irisgps.tracker.R;
import jmt.irisgps.tracker.model.Jmstore;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class OkHttp {
//    public static String URL="http://192.168.1.8";
//    public static String URL="http://jmwebserver.ddns.net";
    public static String URL="http://ptracker.irisgps.com";
    Context ctx;
    Jmstore js;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final OkHttpClient client = new OkHttpClient();
    public OkHttp(Activity ctx){
        this.ctx = ctx;
        js = new Jmstore(ctx);
    }
    public OkHttp(Context ctx){
        this.ctx = ctx;
        js = new Jmstore(ctx);
    }
    public Response makeGetSynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        Response r = client.newCall(request).execute();
        return r;
    }
    public void makeGetASynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        client.newCall(request).enqueue(new Callback(){
            @Override
            public void onFailure(Request request, IOException e) {}
            @Override public void onResponse(Response response) throws IOException {}
        });
    }
    Response makePostRequest(String url, String json) throws IOException {
        RequestBody body2 =  RequestBody.create(JSON, json);
        Request request = prepare_request(url).post(body2).build();
        return client.newCall(request).execute();
    }
    public Request.Builder prepare_request(String url){
        String gcm_id = js.get(S.GCMID);
        System.out.println("----------CK--------");
        System.out.println(js.get(S.CookieIris)+";"+js.get(S.CookieRemember));
        System.out.println(js.get(S.TOKEN));
        System.out.println(js.get(S.IMEI));
        System.out.println(gcm_id);
        System.out.println("------------------");
        return new Request.Builder()
            .url(url)
            .addHeader(S.UserAgent, S.UserAgentVal)
            .addHeader(S.Cookie, js.get(S.CookieIris)+";"+js.get(S.CookieRemember))
            .addHeader(S.TOKEN, js.get(S.TOKEN))
            .addHeader(S.Device_Type, S.Device_Type_Val)
            .addHeader(S.IMEI, js.get(S.IMEI))
            .addHeader(S.GCMID, gcm_id);
    }
    public ResponseE showResponse(Response response) {
        ResponseE ss = new ResponseE();
        try {
            String in = response.body().string();
            System.out.println("------>>>>");
//            new Util(ctx).saveFile(in);
            System.out.println(in);
            ss.setSuccess(response.isSuccessful());
            if(!response.isSuccessful()){
                ss.setMassege("Error del servidor");
            }else{
                JSONObject jo = new JSONObject(in);
                String code = jo.getString("code");
                ss.setCode(code);
                if(code.equals(S.RESPONSE.irs401)||code.equals(S.RESPONSE.irs_invalid_data)||code.equals(S.RESPONSE.irs2001)){
//                System.out.println("A cerrar sesion....!!!");
                    ss.setSuccess(false);
                    //TODO Usar en caso sea necesario cerrar sesion emergente.
//                new Util(ctx).toLogin();
//                return ss;
                }
                ss.setData(jo.getString("data"));
                ss.setMassege(jo.getString("message"));
            }
            Headers responseHeaders = response.headers();
            for (int i = 0; i < responseHeaders.size(); i++){
//                System.out.println("000:"+responseHeaders.name(i)+"="+responseHeaders.value(i));
                if(responseHeaders.name(i).equals(S.TOKEN)){
                    js.push(S.TOKEN, responseHeaders.value(i));
                }
                if(responseHeaders.name(i).equals(S.Set_Cookie)){
                    if(responseHeaders.value(i).startsWith("cobra_irisgps_cookie"))
                        js.push(S.CookieIris, responseHeaders.value(i));
                    else if(responseHeaders.value(i).startsWith("remember_"))
                        js.push(S.CookieRemember, responseHeaders.value(i));
                }
            }

        } catch (IOException ex) {
            ss.setMassege("No hay data");
            ss.setSuccess(false);
            return ss;
        }catch (JSONException e) {
            e.printStackTrace();
            ss.setMassege("No es JSON");
            ss.setSuccess(false);return ss;
        }
        return ss;
    }
    public ResponseE makeGetRequest(String service) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makeGetSynchronous(service);
            return showResponse(r);
        }catch (IOException e){
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.irs_without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
    public ResponseE makePostRequest(String service,JSONObject params) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makePostRequest(service,params.toString());
            return showResponse(r);
        }catch (IOException e){
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.irs_without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
}