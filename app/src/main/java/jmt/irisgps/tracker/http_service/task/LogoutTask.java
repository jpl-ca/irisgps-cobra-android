package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.os.AsyncTask;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.model.entity.ResponseE;


public class LogoutTask extends AsyncTask<String, String, ResponseE> {
    Activity ctx;
    public LogoutTask(Activity ctx){
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            Z = new OkHttp(ctx).makeGetRequest("/ws/auth/logout");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
}