package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import org.json.JSONObject;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.DA.LocationHistoryDA;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class SendBatchLocationTask extends AsyncTask<String, String, ResponseE>{
    Context ctx;
    CallbackRequest delegate;
    LocationHistoryDA locationDA;
    public SendBatchLocationTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        locationDA = new LocationHistoryDA();
    }
    public SendBatchLocationTask(Context ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        locationDA = new LocationHistoryDA();
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        String locations = locationDA.selectJson();
        try{
            JSONObject jo = new JSONObject();
            jo.put(S.POSITION.locations,locations);
//            jo.put(S.TASK.route_task_id,locations);
//            jo.put(S.POSITION.locations,locations);
//            System.out.println(jo.toString());
            Z = new OkHttp(ctx).makePostRequest("/ws/tracking-route/store-locations",jo);
            System.out.println("-----|>");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE result){
        if(result!=null&&result.isSuccess()){
            locationDA.clear();
            delegate.processFinish(result);
        }
        else {
            System.out.println(result.getData()+" ");
            delegate.processFinish();
        }
    }
}