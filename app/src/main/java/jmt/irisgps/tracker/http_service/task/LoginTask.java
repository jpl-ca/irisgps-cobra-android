package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import org.json.JSONObject;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class LoginTask extends AsyncTask<String, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    public LoginTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject();
            jo.put(S.LOGIN.email,params[0]);
            jo.put(S.LOGIN.password,params[1]);
            Z = new OkHttp(ctx).makePostRequest("/ws/auth/login",jo);
            System.out.println("-----|>");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        if(response.isSuccess()){
            delegate.processFinish(response);
        }else{
            if(response.getCode()==S.RESPONSE.irs_without_connection)
                Toast.makeText(ctx, response.getMassege(), Toast.LENGTH_LONG).show();
            delegate.processFinish();
        }
    }
}