package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.DA.TasksDA;
import jmt.irisgps.tracker.model.DA.UserAccountDA;
import jmt.irisgps.tracker.model.Jmstore;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class LoadRouteInfoTask extends AsyncTask<String, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    GoogleCloudMessaging gcm;
    String regId;
    Jmstore jmstore;
    public LoadRouteInfoTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        regId="";
        gcm = GoogleCloudMessaging.getInstance(this.ctx);
        jmstore = new Jmstore(ctx);
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try {
            Z = new OkHttp(ctx).makeGetRequest("/ws/tracking-route/route-info");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        if(response.isSuccess()){
            try {
                JSONObject jo = new JSONObject(response.getData());
                System.out.println("El User ES:");
                System.out.println(response.getData());
                System.out.println("-----------------------------");
                System.out.println(jo.toString());
                System.out.println(jo.getString(S.VAR.user));
                JSONObject joUser = new JSONObject(jo.getString(S.VAR.user));
                joUser.put(S.VAR.tracking_route_id,jo.get(S.VAR.id));
                new UserAccountDA().insert(joUser.toString());
                new TasksDA().insert(jo.getString(S.VAR.tasks));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            delegate.processFinish(response);
        }else delegate.processFinish();
    }
}