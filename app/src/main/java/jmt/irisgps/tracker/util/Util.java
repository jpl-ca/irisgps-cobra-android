package jmt.irisgps.tracker.util;

import android.app.Activity;
import android.content.Intent;
import android.text.format.DateFormat;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import jmt.irisgps.tracker.SplashActivity;
import jmt.irisgps.tracker.http_service.task.LogoutTask;
import jmt.irisgps.tracker.model.DA.UserAccountDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;

/**
 * Created by JMTech-Android on 22/05/2015.
 */
public class Util {
    Activity activity;
    public Util(Activity activity){
        this.activity = activity;
    }
    public void finalizarSesion(boolean to_login){
//        new UserAccountDA().logout();
        new LogoutTask(activity).execute();
        if(to_login)
            toLogin();
        else activity.finish();
    }
    public void toLogin(){
//        new UserAccountDA().delete();
        Intent it = new Intent(activity, SplashActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(it);
        activity.finish();
    }
    public static HashMap<Integer,String> getStateTask(){
        HashMap<Integer,String> hm = new HashMap<>();
        hm.put(1,S.TASK_STATE.Programada);
        hm.put(2,S.TASK_STATE.Realizada);
        hm.put(3,S.TASK_STATE.Pospuesta);
        hm.put(4,S.TASK_STATE.Cancelada);
        hm.put(5,S.TASK_STATE.Reprogramar);
        return hm;
    }
    public static String now(){
        return (String) DateFormat.format("yyyy-MM-dd kk:mm:ss", new Date());
    }
    private static double calcDistance(double orig_lat, double orig_lng, double lat, double lng) {
        double distance = 6371 * (2 * Math.atan2(Math.sqrt((Math.sin(((lat - orig_lat) * Math.PI / 180)/2) * Math.sin(((lat - orig_lat) * Math.PI / 180)/2) + Math.sin(((lng - orig_lng) * Math.PI / 180)/2) * Math.sin(((lng - orig_lng) * Math.PI / 180)/2) * Math.cos((orig_lat * Math.PI / 180)) * Math.cos((lat * Math.PI / 180)))), Math.sqrt(1-(Math.sin(((lat - orig_lat) * Math.PI / 180)/2) * Math.sin(((lat - orig_lat) * Math.PI / 180)/2) + Math.sin(((lng - orig_lng) * Math.PI / 180)/2) * Math.sin(((lng - orig_lng) * Math.PI / 180)/2) * Math.cos((orig_lat * Math.PI / 180)) * Math.cos((lat * Math.PI / 180))))))*1000;
        return distance;
    }
    private static double calcAngle(double latN,double  lngN,double  latN1,double  lngN1,double  latN2,double  lngN2){
        LatLng vAlfa = new LatLng((latN2 - latN1), (lngN2 - lngN1));
        LatLng vBeta = new LatLng((latN - latN1), (lngN - lngN1));
        double angle = Math.acos((vAlfa.latitude * vBeta.latitude + vAlfa.longitude * vBeta.longitude) / (Math.sqrt(Math.pow(vAlfa.latitude,2) + Math.pow(vAlfa.longitude,2)) * Math.sqrt(Math.pow(vBeta.latitude, 2) + Math.pow(vBeta.longitude, 2))));
        return angle*180/Math.PI;
    }
    public static ArrayList<LocationHistoriesE> posCorrectionC(List<LocationHistoriesE> pox,double minDistance){
        int tam = pox.size();// TOTAL DE POSICIONES REGISTRADAS
        ArrayList<LocationHistoriesE> res = new ArrayList<>();// RESULTADO
        LocationHistoriesE ce;
        LocationHistoriesE le,leSub;
        String ree = "";
        double dis,angle;
        for (int i = 0; i < tam; i++) {
            ce = pox.get(i);
            if(i==0||ce.getIncident_type_id()!=null){
                res.add(ce);
                System.out.println("lat: "+ce.getLatitud()+" lng: "+ce.getLongitud()+" distancia: 0 entro: SI");
            }else{
                le = res.get(res.size()-1);
                dis = calcDistance(le.getLatitud(), le.getLongitud(), ce.getLatitud(), ce.getLongitud());
                if(res.size() >= 3){
                    leSub = res.get(res.size()-2);
                    angle = calcAngle(ce.getLatitud(), ce.getLongitud(), le.getLatitud(), le.getLongitud(), leSub.getLatitud(), leSub.getLongitud());
                    if(dis >= minDistance){
//                        if(angle <= 18){
                        if((angle <= 18)&&res.get(res.size() - 1).getIncident_type_id()!=null){
                            res.remove(res.size() - 1);
                            ree = "NO y quitamos el anterior, siendo el angulo: "+angle;
                        }else{
                            res.add(pox.get(i));
                            ree = "SI y el angulo es: "+angle;
                        }
                    }
                }else{
                    if(dis >= minDistance){
                        res.add(pox.get(i));
                        ree = "SI";
                    }else{
                        ree="NO";
                    }
                }
                System.out.println("lat: "+ce.getLatitud()+" lng: "+ce.getLongitud()+" distancia: "+ dis +" entro: "+ree);
            }
        }
        System.out.println("puntos originales: " + pox.size());
        System.out.println("puntos corregidos: " + res.size());
        return res;
    }
}