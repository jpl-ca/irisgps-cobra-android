package jmt.irisgps.tracker.model.DA;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.UserAccountE;
import jmt.irisgps.tracker.util.Util;
/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class LocationHistoryDA {
    Gson gson;
    public LocationHistoryDA(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
    public int insert(LocationHistoriesE position){
        String currentTime = Util.now();
        UserAccountE usr = new UserAccountDA().select();
        if(usr==null)return 0;
        position.setUser_id(usr.getUser_id());
        if(usr.getTracking_route_id()>0)
             position.setTracking_route_id(String.valueOf(usr.getTracking_route_id()));
        else position.setTracking_route_id(null);
        position.setCreated_at(currentTime);
        position.setUpdated_at(currentTime);
        position.save();
        System.out.println("*********NEW ROUTE********************"+currentTime);
//        System.out.println(usr.getTracking_route_id());
//        System.out.println(position.getTracking_route_id());
        System.out.println(gson.toJson(position));
        return Count();
    }

    public int Count(){
        return LocationHistoriesE.listAll(LocationHistoriesE.class).size();
    }

    public List<LocationHistoriesE> select(){
        List<LocationHistoriesE> positions = LocationHistoriesE.listAll(LocationHistoriesE.class);
        return positions;
    }
    public LocationHistoriesE last(){
        List<LocationHistoriesE> all = select();
        if(all.size()>0){
            return all.get(all.size()-1);
        }
        return null;
    }

    public String selectJson(){
        List<LocationHistoriesE> positions = LocationHistoriesE.listAll(LocationHistoriesE.class);
        return gson.toJson(positions);
    }
    public String selectJson(List<LocationHistoriesE> positions){
        return gson.toJson(positions);
    }
    public void clear(){
        System.out.println("Deleting Locations");
        LocationHistoriesE.deleteAll(LocationHistoriesE.class);
    }
}