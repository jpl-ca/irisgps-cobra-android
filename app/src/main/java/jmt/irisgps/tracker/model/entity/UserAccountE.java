package jmt.irisgps.tracker.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 21/05/2015.
 */
public class UserAccountE extends SugarRecord<UserAccountE> implements Serializable{
    @Expose
    @SerializedName("id")
    long user_id;
    @Expose
    String email;
    @Expose
    String first_name;
    @Expose
    String last_name;
    @Expose
    long device_id;
    @Expose
    long tracking_route_id;
    boolean routes_completed;
    public UserAccountE(){}

    public long getTracking_route_id() {
        return tracking_route_id;
    }

    public void setTracking_route_id(long tracking_route_id) {
        this.tracking_route_id = tracking_route_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public long getDevice_id() {
        return device_id;
    }

    public boolean isRoutes_completed() {
        return routes_completed;
    }

    public void setRoutes_completed(boolean routes_completed) {
        this.routes_completed = routes_completed;
    }
}