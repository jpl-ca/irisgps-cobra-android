package jmt.irisgps.tracker.model.entity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class LocationHistoriesE extends SugarRecord<LocationHistoriesE> {
    @Expose
    @SerializedName("id")
    private long location_id;
    @Expose
    private String tracking_route_id;
    @Expose
    private long user_id;
    @Expose
    private Double lat;
    @Expose
    private Double lng;
    @Expose
    private String incident_type_id;
    @Expose
    private String incident_description;
    @Expose
    private String created_at;
    @Expose
    private String updated_at;
    public LocationHistoriesE(){}
    public LocationHistoriesE(Double lat, Double lng){
        this.incident_type_id = null;
        this.incident_description = null;
        this.lat = lat;
        this.lng = lng;
    }
    public LocationHistoriesE(Double lat, Double lng, String incident_type_id, String incident_description){
        this.lat = lat;
        this.lng = lng;
        this.incident_type_id = incident_type_id;
        this.incident_description = incident_description;
    }

    public long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(long location_id) {
        this.location_id = location_id;
    }

    public String getTracking_route_id() {
        return tracking_route_id;
    }

    public void setTracking_route_id(String tracking_route_id) {
        this.tracking_route_id = tracking_route_id;
    }

    public Double getLatitud() {
        return lat;
    }

    public void setLatitud(Double lat) {
        this.lat = lat;
    }

    public Double getLongitud() {
        return lng;
    }

    public void setLongitud(Double lng) {
        this.lng = lng;
    }

    public String getIncident_type_id() {
        return incident_type_id;
    }

    public void setIncident_type_id(String incident_type_id) {
        this.incident_type_id = incident_type_id;
    }

    public String getIncident_description() {
        return incident_description;
    }

    public void setIncident_description(String incident_description) {
        this.incident_description = incident_description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}