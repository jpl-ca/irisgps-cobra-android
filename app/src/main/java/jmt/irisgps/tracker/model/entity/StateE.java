package jmt.irisgps.tracker.model.entity;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 11/06/2015.
 */
public class StateE extends SugarRecord<StateE> implements Serializable {
    @Expose
    private String name;
    public StateE(){}
    public StateE(String name){this.name=name;}
}