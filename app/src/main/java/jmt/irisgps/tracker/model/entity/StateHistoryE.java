package jmt.irisgps.tracker.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 11/06/2015.
 */
public class StateHistoryE extends SugarRecord<StateHistoryE> implements Serializable {
    @Expose
    @SerializedName("id")
    private long state_id;
    @Expose
    private long task_state_id;
    @Expose
    private String description;
    @Expose
    private StateE state;
    @Expose
    private long route_task_id;
    @Expose
    @SerializedName("created_at")
    private String fecha;

    public long getState_id() {
        return state_id;
    }

    public void setState_id(long state_id) {
        this.state_id = state_id;
    }

    public long getTask_state_id() {
        return task_state_id;
    }

    public void setTask_state_id(long task_state_id) {
        this.task_state_id = task_state_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StateE getState() {
        return state;
    }

    public void setState(StateE state) {
        this.state = state;
    }

    public long getRoute_task_id() {
        return route_task_id;
    }

    public void setRoute_task_id(long route_task_id) {
        this.route_task_id = route_task_id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
