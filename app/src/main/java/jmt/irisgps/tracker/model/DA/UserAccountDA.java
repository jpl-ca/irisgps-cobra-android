package jmt.irisgps.tracker.model.DA;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import jmt.irisgps.tracker.model.entity.UserAccountE;
/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class UserAccountDA {
    Gson gson;
    public UserAccountDA(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
    public void insert(String jo){
        System.out.println("Save :"+jo);
        UserAccountE user= gson.fromJson(jo,UserAccountE.class);
        user.save();
    }
    public void insert(UserAccountE user_account){
        user_account.save();
    }
    public UserAccountE select(){
        try {
            List<UserAccountE> user_accounts = UserAccountE.listAll(UserAccountE.class);
            return user_accounts.get(0);
        }catch (Exception e){
            return null;
        }
    }
    public int Count(){
        return UserAccountE.listAll(UserAccountE.class).size();
    }
    public void delete(){
        UserAccountE.deleteAll(UserAccountE.class);
    }

    public void logout() {
        if(Count()>0){
            UserAccountE user = select();
            user.deleteAll(UserAccountE.class);
//            user.setTracking_route_id(0);
        }
    }
}