package jmt.irisgps.tracker;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//import jmt.irisgps.cobratrack.dialog.InfoPersonalDialog;
import jmt.irisgps.tracker.dialog.RegisterIncidentDialog;
import jmt.irisgps.tracker.http_service.task.ChangeStateTaskTask;
import jmt.irisgps.tracker.interfaz.CallbackDialog;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.DA.TasksDA;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.model.entity.StateE;
import jmt.irisgps.tracker.model.entity.StateHistoryE;
import jmt.irisgps.tracker.model.entity.TasksE;
import jmt.irisgps.tracker.service.LocationFusedService;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class TaskCustomerActivity extends ActionBarActivity implements View.OnClickListener{
    Toolbar toolbar;
    Util util;
    TasksDA taskDA;
    TasksE task;
    EditText txt_comentario;
    TextView txt_name,txt_detalle,txt_direccion,txt_num_telefono;
    Button btnCall,btnEnviarEstado,btnCancelarEnvio;
    LinearLayout lv_history_state,ll_estado_tarea;
    Spinner spinner;
    HashMap<Integer,String> hm;
    LayoutInflater inflater;
    long route_task_id;
    List<StateHistoryE> SH;
    LatLng ME;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_customer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  "+getString(R.string.s_cliente));
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.ic_toolbar);
        util = new Util(this);
        Bundle extra = getIntent().getExtras();
        long id = extra.getLong(S.TASK.id);
        route_task_id = extra.getLong(S.TASK.route_task_id);
        ME = new LatLng(extra.getDouble(S.POSITION.latitude),extra.getDouble(S.POSITION.longitude));
        taskDA = new TasksDA();
        task = taskDA.selectById(id);
        txt_comentario = (EditText) findViewById(R.id.txt_comentario);
        spinner = (Spinner) findViewById(R.id.sp_state);
        txt_name = (TextView)findViewById(R.id.txt_name);
        txt_detalle = (TextView)findViewById(R.id.txt_detalle);
        txt_direccion = (TextView)findViewById(R.id.txt_direccion);
        txt_num_telefono = (TextView)findViewById(R.id.txt_num_telefono);
        lv_history_state = (LinearLayout)findViewById(R.id.lv_historial_estados);
        ScrollView sView = (ScrollView)findViewById(R.id.scroll);
        btnCall = (Button)findViewById(R.id.btnCall);
        btnCall.setOnClickListener(this);
        btnEnviarEstado = (Button)findViewById(R.id.btnEnviarEstado);
        btnEnviarEstado.setOnClickListener(this);
        btnCancelarEnvio = (Button)findViewById(R.id.btnCancelarEnvio);
        btnCancelarEnvio.setOnClickListener(this);
        ll_estado_tarea = (LinearLayout)findViewById(R.id.ll_estado_tarea);
        sView.setVerticalScrollBarEnabled(false);
        sView.setHorizontalScrollBarEnabled(false);
        hm = Util.getStateTask();
        inflater = LayoutInflater.from(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        addTypeStates();
        txt_name.setText(task.getCustomer().getName());
        txt_direccion.setText(task.getCustomer().getAddress());
        txt_num_telefono.setText(task.getCustomer().getPhone());
        txt_detalle.setText(task.getDescription());
        SH = task.getState_history();
        showStateHistory();
    }
    public void showStateHistory(){
        if(task.getTask_state_id()==S.TASK_STATE.REALIZADA
                ||task.getTask_state_id()==S.TASK_STATE.REPROGRAMAR
                ||task.getTask_state_id()==S.TASK_STATE.CANCELADA)
            ll_estado_tarea.setVisibility(View.GONE);
        lv_history_state.removeAllViews();
        for(int i=SH.size()-1;i>=0;i--)
            addStateH(SH.get(i));
    }
    public void addStateH(StateHistoryE stateH){
        View view  = inflater.inflate(R.layout.listview_state, lv_history_state, false);
        ((TextView)view.findViewById(R.id.tv_estado)).setText(hm.get((int)stateH.getTask_state_id()));
        ((TextView)view.findViewById(R.id.tv_descripcion)).setText(stateH.getDescription());
        String fecha = stateH.getFecha().replace("/","-");
        ((TextView)view.findViewById(R.id.tv_fecha)).setText(fecha.substring(0, fecha.length() - 3));
        lv_history_state.addView(view);
    }
    public void addTypeStates(){
        String[] values = new String[hm.size()+1];
        Object aSt[] = hm.keySet().toArray();
        Arrays.sort(aSt);
        values[0] = S.VAR.seleccione;
        for (int i=0;i<aSt.length;i++){
            values[i+1] = hm.get(aSt[i]);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_track_map, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_salir) {
            new Util(this).finalizarSesion(true);
            stopService(new Intent(this, LocationFusedService.class));
            return true;
        }
        if (id == R.id.action_incident) {
            registroIncidente();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void registroIncidente() {
        new RegisterIncidentDialog(this).show(this,ME,new CallbackDialog() {
            @Override
            public void finish(String... params) {
                System.out.println("This a greeting!!");
            }
        });
    }
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnCall){
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + String.valueOf(task.getCustomer().getPhone())));
            startActivity(intent);
        }else if(v.getId()==R.id.btnEnviarEstado){
            final String comen = txt_comentario.getText().toString();
            final int pos = spinner.getSelectedItemPosition();
            if(pos==0){
                Toast.makeText(this, getString(R.string.s_seleccione_un_estado), Toast.LENGTH_LONG).show();
            }else if(comen.length()<3){
                Toast.makeText(this, getString(R.string.s_es_mecesario_un_comentario), Toast.LENGTH_LONG).show();
            }else{
                txt_comentario.setEnabled(false);
                btnEnviarEstado.setEnabled(false);
                new ChangeStateTaskTask(this,new CallbackRequest() {
                    @Override
                    public void processFinish() {
                        btnEnviarEstado.setEnabled(true);
                        txt_comentario.setEnabled(true);
                    }
                    @Override
                    public void processFinish(ResponseE response) {
                        btnEnviarEstado.setEnabled(true);
                        txt_comentario.setEnabled(true);
                        final String currentTime = Util.now();
                        StateHistoryE st = new StateHistoryE();
                        st.setRoute_task_id(route_task_id);
                        st.setTask_state_id(pos);
                        st.setDescription(comen);
                        st.setState(new StateE(spinner.getSelectedItem().toString()));
                        st.setFecha(currentTime);
                        st.save();

                        st.setRoute_task_id(route_task_id);
                        ArrayList<StateHistoryE> state = new ArrayList<>();
                        state.add(st);
                        task.setState_history(state);
                        task.setTask_state_id(pos);
                        task.save();
                        txt_comentario.setText("");
                        SH.add(st);
                        showStateHistory();
                    }
                }).execute(String.valueOf(route_task_id),String.valueOf(pos),comen);
            }
        }else if(v.getId()==R.id.btnCancelarEnvio){
            txt_comentario.setText("");
        }
    }
}